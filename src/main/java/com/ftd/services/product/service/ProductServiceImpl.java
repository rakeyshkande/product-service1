/**
 * 
 */
package com.ftd.services.product.service;

import com.ftd.services.product.helper.ProductServiceHelper;
import com.ftd.services.product.util.JsonUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author djohn
 *
 */
@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	ProductServiceHelper productServiceHelper;

	@Autowired
	JsonUtil jsonUtil;

	@Override
	public String getProductDetails(List<String> ids) {
		List<String> productDetailsList  = new ArrayList<>();
		if(CollectionUtils.isNotEmpty(ids)) {
			ids.forEach( id -> {
				String productDetailsIndividual = productServiceHelper.getProductData(id);
				if(StringUtils.isNotEmpty(productDetailsIndividual))
					productDetailsList.add(productDetailsIndividual);
				}
			);
		}
		return jsonUtil.buildArray(productDetailsList);
	}
}
