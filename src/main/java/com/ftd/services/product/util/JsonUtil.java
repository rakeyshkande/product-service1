/**
 * 
 */
package com.ftd.services.product.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftd.services.product.constants.CommonConstants;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author djohn
 *
 */
@Service
public class JsonUtil {
	
	private ObjectMapper objectMapper = new ObjectMapper();

	public String asJsonString(final Object obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public String buildArray(List<String> list) {
		StringBuilder reponse = new StringBuilder("[");

		if (CollectionUtils.isNotEmpty(list))
			list.forEach(str -> reponse.append(str).append(","));

		// remove the trailing comma separator
		if (reponse.length() > 1)
			reponse.deleteCharAt(reponse.length() - 1);

		reponse.append("]");

		return reponse.toString();
	}

    public String readJsonFromFile(String filename) {
        String data = null;

        StringBuilder fileName = new StringBuilder(CommonConstants.DATA_LOCATION);
        fileName.append(filename);

        try {
            //Get file from resources folder
            ClassLoader classLoader = getClass().getClassLoader();
            URL url = classLoader.getResource(fileName.toString());
            if(url != null) {
                File file = new File(url.getFile());
                byte[] jsonData = Files.readAllBytes(Paths.get(file.getPath()));
                data = new String(jsonData);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return data;
    }


}
