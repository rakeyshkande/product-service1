package com.ftd.services.product.controller;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.ftd.services.product.service.ProductService;
import com.services.micro.commons.logging.annotation.LogExecutionTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author djohn
 *
 */
@RestController
@RequestMapping("/api")
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@RequestMapping(value = { "/products/{ids}" }, method = { RequestMethod.GET }, produces = "application/json", headers={"accept=application/json"})
	@Timed
	@ExceptionMetered
	@LogExecutionTime
	public String getProductDetails(@PathVariable("ids") String ids)  {
		List<String> productIds = new ArrayList<>();
		StringTokenizer tokens = new StringTokenizer(ids, ",");
		while (tokens.hasMoreTokens()) {
			productIds.add(tokens.nextToken());
		}
		return productService.getProductDetails(productIds);
		
	}

	

}
