package com.ftd.services.product.controller;

import com.ftd.services.product.ProductServiceApp;
import com.ftd.services.product.helper.ProductServiceHelper;
import com.ftd.services.product.service.ProductService;
import com.ftd.services.product.util.JsonUtil;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    @MockBean
    private ProductService productService;

    @Autowired
    WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ProductServiceHelper productServiceHelper;

    @Autowired
    private JsonUtil jsonUtil;

    // Stub
    private List<String> mockProducts;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(documentationConfiguration(this.restDocumentation))
                .build();

        mockProducts = Arrays.asList(productServiceHelper.getProductData("960"),
                                     productServiceHelper.getProductData("970"));
    }

    @Test
    public void ensureMockMvcIsInjected() throws Exception {
        assertNotNull(mockMvc);
    }

    @Test
    public void ensureProductServiceHelperIsInjected() throws Exception {
        assertNotNull(productServiceHelper);
    }

    @Test
    public void getProductDetails_singleProduct() throws Exception {
        //select test data
        String jsonString_960 = mockProducts.get(0);
        List<String> productIds = Arrays.asList("960");
        String expectedJsonResponse = jsonUtil.buildArray(Arrays.asList(jsonString_960));

        when(productService.getProductDetails(productIds)).thenReturn(expectedJsonResponse);

        MvcResult result = this.mockMvc.perform(get("/api/products/{ids}", String.join(",", productIds))
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andDo(document("querySingleProduct"))
                .andReturn();

        verify(productService, times(1)).getProductDetails(productIds);
        assertThat(result.getResponse().getContentAsString(), is(expectedJsonResponse));
    }

    @Test
    public void getProductDetails_multipleProducts() throws Exception {
        //select test data
        String jsonString_960 = mockProducts.get(0);
        String jsonString_970 = mockProducts.get(1);
        List<String> productIds = Arrays.asList("960", "970");
        String expectedJsonResponse = jsonUtil.buildArray(Arrays.asList(jsonString_960, jsonString_970));

        when(productService.getProductDetails(productIds)).thenReturn(expectedJsonResponse);

        MvcResult result = this.mockMvc.perform(get("/api/products/{ids}", String.join(",", productIds))
                .accept(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andDo(document("queryMultipleProducts"))
                .andReturn();

        verify(productService, times(1)).getProductDetails(productIds);
        assertThat(result.getResponse().getContentAsString(), is(expectedJsonResponse));
    }

}