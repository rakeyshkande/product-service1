FROM openjdk:8
MAINTAINER ftd
ADD target/*.jar /appl/productservice/jar/
ADD src/main/resources /appl/productservice/config/
ADD startup.sh /appl/productservice/bin/entrypoint.sh
ENV PATH /:$PATH
RUN chmod +x /appl/productservice/bin/entrypoint.sh
ENTRYPOINT ["/appl/productservice/bin/entrypoint.sh"]